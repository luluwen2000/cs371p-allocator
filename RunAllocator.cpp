#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <cassert>

using namespace std;

/*
 *  Provides debug output utilities.
 *  Samuel Laberge 2021
 */

#ifndef DEBUG_H
#define DEBUG_H

#include <iostream>

/* 
 * Comment out the following line to remove debugging output
 * ~~ This is essentially the on/off switch!  ~~
 */
 
// #define DEBUG_ON

/* Adds color to the output */
#define DEBUG_COLOR "\033[1;33m" // Bold Yellow
/* Resets terminal colors */
#define COLOR_RESET "\033[0m"

/* 
 * Added to the beginning of all debug output
 * Includes a "DEBUG" prefix followed by the file and line
 * number the output is coming from 
 */
#define DEBUG_PREFIX ""

#ifdef DEBUG_ON
 /* Example usage:
  * DEBUG << "This is debug output!" << std::endl;
  */
    #define DEBUG std::cout << DEBUG_PREFIX
#else
    /* If DEBUG is not defined, the line becomes a no-op by magic of operator precedence
   * and short-circuiting!
   */
    #define DEBUG 0 && std::cout
#endif

#endif // DEBUG_H

void print_vector(vector<int>& vector) {
    DEBUG << "[";
    for (size_t i = 0; i < vector.size(); i++) {
        DEBUG << " " << vector[i] << " ";
    }
    DEBUG << "]";
    DEBUG << endl;
}

void print_blocks(vector<int>& blocks) {
    for (size_t i = 0; i < blocks.size(); i++) {
        cout << blocks[i];
        if (i != blocks.size() - 1) {
            cout << " ";
        }
    }
    cout << endl;
}

// assign free block to request after finding the free block to assign
void allocate(vector<int>& blocks, int size_to_allocate, int free_block_index) {
    
    // change the value at free_block_index
    int free_block_size = blocks[free_block_index];
    
    // if fits perfectly
    if (free_block_size == size_to_allocate) {
        blocks[free_block_index] = -blocks[free_block_index];
        return;
    }
    
    int bytes_left = free_block_size - size_to_allocate - 8;
    
    if (bytes_left == 0) {
        // no use for it, just merge it with the allocated block
        blocks[free_block_index] = -blocks[free_block_index];
        return;
    }
    
    blocks[free_block_index] = bytes_left;
    // insert size_to_allocate at free_block_index
    // inserts 7 at i-th index 
    blocks.insert(blocks.begin() + free_block_index, -size_to_allocate); 
}

// returns if the allocation is successful. 
bool find_and_allocate_block(vector<int>& blocks, int size_to_allocate) {
    DEBUG << "allocating..." << endl;
    print_vector(blocks);
    DEBUG << "size_to_allocate: " << size_to_allocate << endl;
    for (size_t i = 0; i < blocks.size(); i++) {
        if (blocks[i] < 0) {
            // unfree block, skip
            continue;
        }
        if (blocks[i] >= size_to_allocate) {
            // do allocate here
            allocate(blocks, size_to_allocate, i);
            
            print_vector(blocks);
            DEBUG << "done allocating!" << endl << endl;
            return true;
        }
    }
    return false;
}

// deallocate at index_to_free (assume 0 indexing)
bool find_and_deallocate(vector<int>& blocks, int index_of_busy_block) {
    DEBUG << "deallocating..." << endl;
    print_vector(blocks);
    int index_to_free = -1;
    // find the busy bock
    for (size_t i = 0; i < blocks.size(); i++) {
        if (blocks[i] < 0) {
            index_of_busy_block--;
            if (index_of_busy_block == 0) {
                // found. break.
                index_to_free = i;
                break;
            }
        }
    }
    
    if (index_to_free == -1) {
        DEBUG << "error: index_of_busy_block not found in blocks vector" << endl;
        DEBUG << "index_of_busy_block: " << index_of_busy_block << endl;
        return false;
    }
    
    DEBUG << "index_to_free: " << index_to_free << endl;
    
    int bytes_to_free = blocks[index_to_free];
    // must be less than 0 (a busy block)
    if (bytes_to_free > 0) {
        DEBUG << "error: bytes_to_free is already freed" << endl;
        DEBUG << "index_to_free: " << index_to_free << endl;
        DEBUG << "bytes_to_free: " << bytes_to_free << endl;
        return false;
    }
    
    // change it to positive, a free block
    blocks[index_to_free] = -bytes_to_free;
    
    // merge free blocks to the right
    int right_index = index_to_free + 1;
    while (right_index < (int) blocks.size() && blocks[right_index] > 0) {
        blocks[index_to_free] += blocks[right_index] + 8;   
        blocks.erase(blocks.begin() + right_index);
    }
    
    // merge free blocks to the left
    int left_index = index_to_free - 1;
    while (left_index >= 0 && blocks[left_index] > 0) {
        blocks[index_to_free] += blocks[left_index] + 8;
        blocks.erase(blocks.begin() + left_index);
        index_to_free--;
        left_index = index_to_free - 1;
    }
    
    print_vector(blocks);
    DEBUG << "done deallocating!" << endl << endl;
    return true;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int num_cases;
    vector<int> blocks;
    cin >> num_cases;
    cin.get();
    cin.get();
    
    for (int i = 0; i < num_cases; i++) {
        // clear blocks for each new case
        blocks.clear();
        blocks.push_back(992);
        
        string line;
        while (getline(cin, line)) {
            if (line.empty()) {
                break;
            }
            int num = stoi(line);
            // allocate
            if (num > 0) {
                // find a free block that will fit
                if (! find_and_allocate_block(blocks, num * 8)) {
                    // no free block that will fit. throw bad allocation error
                    DEBUG << "No free block can fit. Bad allocation" << endl;
                }
            }
            // deallocate
            else {
                find_and_deallocate(blocks, -num);
            }
        }
        print_blocks(blocks);
    }
    return 0;
}
